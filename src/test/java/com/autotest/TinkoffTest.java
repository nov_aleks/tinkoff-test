package com.autotest;

import elements.MainMenuElement;
import elements.RegionElement;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.CommunalPaymentsPage;
import pages.PaymentsPage;
import pages.ZhkuMoscowPage;
import validators.MainMenuValidators;
import validators.PaymentsValidators;
import validators.ZhkuMoscowValidators;


import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TinkoffTest {

    private static String url;
    private static String chrome_driver;
    private String messageElement = "The element name does not match the expected";
    private String messageUrl     = "Current url does not match the expected";

    private static WebDriver driver;
    private MainMenuElement mainMenuElement;
    private PaymentsPage paymentsPage;
    private CommunalPaymentsPage communalPaymentsPage;
    private RegionElement regionElement;
    private ZhkuMoscowPage zhkuMoscowPage;

    @BeforeClass
    public static void setup() {
        Map<String, String> env = new ProcessBuilder().environment();
        chrome_driver = env.get("CHROME_DRIVER");
        url = env.get("SITE_URL");

        System.setProperty("webdriver.chrome.driver", chrome_driver);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void navigateToZhkhPaymentTest() {
        driver.get(url);
        mainMenuElement = new MainMenuElement(driver);
        MainMenuValidators mainMenuValidators = mainMenuElement.validators;

        // Check main menu
        String errors = mainMenuValidators.isMainMenuCorrect().toString();
        Assert.assertTrue(errors, errors.equals(""));

        // Check current url
        Assert.assertEquals(messageUrl,url + "/", driver.getCurrentUrl());

        //Go to payments page
        mainMenuElement.clickMenuItemBtn("/payments/");

        paymentsPage = new PaymentsPage(driver);
        PaymentsValidators paymentsValidators = paymentsPage.validators;

        //Check payments menu
        errors = paymentsValidators.isPaymentsMenuCorrect(url).toString();
        Assert.assertTrue(errors, errors.equals(""));

        //Go to the kommunalnie-platezhi page
        paymentsPage.clickPaymentsMenuItemBtn("/kommunalnie-platezhi/");

        communalPaymentsPage = new CommunalPaymentsPage(driver);
        regionElement = new RegionElement(driver);

        //Check if region is Moscow (if not: set it)
        if (!communalPaymentsPage.getPaymentsCatalogHeaderName().equals("ЖКХ в Москве")) {
            communalPaymentsPage.clickRegionChangeBtn();
            regionElement.setCity("Москва");
            Assert.assertEquals(messageElement, "ЖКХ в Москве", communalPaymentsPage.getPaymentsCatalogHeaderName());
        }

        // Check current url
        Assert.assertEquals(messageUrl,url + "/payments/categories/kommunalnie-platezhi/", driver.getCurrentUrl());

        // Check if the first provider name is ZHKU-moskva
        Assert.assertEquals(messageElement,"ЖКУ-Москва", communalPaymentsPage.getFirstProviderName("zhku-moskva"));

        // Go to the ZHKU-moskva page
        communalPaymentsPage.clickFirstProviderBtn("zhku-moskva");

        zhkuMoscowPage = new ZhkuMoscowPage(driver);
        ZhkuMoscowValidators zhkuMoscowValidators = zhkuMoscowPage.validators;

        // Check the ZHKU-moskva page
        errors = zhkuMoscowValidators.isDefaultPageCorrect(url).toString();
        Assert.assertTrue(errors, errors.equals(""));

        // Go to theZHKU-moskva payment page
        zhkuMoscowPage.clickZhkuTab("Оплатить ЖКУ в Москве");

        // Check the ZHKU-moskva payment page
        errors = zhkuMoscowValidators.isPaymentTabCorrect(url).toString();
        Assert.assertTrue(errors, errors.equals(""));
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }
}
