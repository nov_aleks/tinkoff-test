package pages;

import locators.ZhkuMoscowLocators;
import org.openqa.selenium.WebDriver;
import validators.ZhkuMoscowValidators;

public class ZhkuMoscowPage extends BasePage {

    public ZhkuMoscowValidators validators;
    public ZhkuMoscowLocators locators;

    public ZhkuMoscowPage(WebDriver driver) {
        super(driver);
        this.locators   = new ZhkuMoscowLocators();
        this.validators = new ZhkuMoscowValidators(this);
    }

    public String getPageTitle() {
        return waiters.waitForElement(locators.getElement(locators.pageTitle))
                      .getText().replace("&nbsp;"," ");
    }

    public String getPageTitle(String title) {
        return waiters.waitForElement(locators.getElement(locators.pageTitleByName, title))
                .getText().replace("&nbsp;"," ");
    }

    public void clickZhkuTab(String title) {
        driver.findElement(locators.getElement(locators.zhkuTabTitle, title)).click();
    }

    public String getActiveTabTitle(String name) {
        return waiters.waitForElement(locators.getElement(locators.zhkuActiveTab, name))
                      .getText().replace("&nbsp;"," ");
    }

    public String getTabTitle(String name) {
        return waiters.waitForElement(locators.getElement(locators.zhkuTab, name))
                      .getText().replace("&nbsp;"," ");
    }
}