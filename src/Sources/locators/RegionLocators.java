package locators;

public class RegionLocators extends BaseLocators {
    public String regionLink = "//div[@data-qa-file='UIRegions' and contains(@class, 'item')]//a/span[contains(text(), '%s')]";
}