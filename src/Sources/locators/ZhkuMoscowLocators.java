package locators;

public class ZhkuMoscowLocators extends BaseLocators {
    public String pageTitle = "//div[@data-qa-file='PageTitleContainer']/div[text()]";

    public String pageTitleByName = "//div[@data-qa-file='PageTitleContainer']/div[text()='%s']";

    public String zhkuTab = "//li[@data-qa-file='TabContainer' and not(contains(@class, 'container_active'))]//span[text()='%s']";

    public String zhkuActiveTab = "//li[@data-qa-file='TabContainer' and contains(@class, 'container_active')]//span[text()='%s']";

    public String zhkuTabTitle = "//li[@data-qa-file='TabContainer']//span[text()='%s']";

    private String payTabForm = "//div[@data-qa-file='PlatformLayout' and contains(@class, 'layoutPageComponent')]" +
                                "//div[@data-qa-file='Container' and not(contains(@class, 'Hidden'))]" +
                                "//form/div[not(contains(@class, 'hidden'))]";

    public String formPlaceholder = payTabForm + "//span[contains(@class, 'placeholder-text') or " +
                                                 "contains(@class, 'label_secondary') and " +
                                                 "not(contains(@class, 'label_focused'))]";

}