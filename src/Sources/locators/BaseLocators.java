package locators;

import org.openqa.selenium.By;

public class BaseLocators {
    public By getElement(String locator) {
        return By.xpath(locator);
    }

    public By getElement(String locator, String arg) {
        return By.xpath(String.format(locator, arg));
    }

    public By getElement(String locator, int arg) {
        return By.xpath(String.format(locator, Integer.toString(arg)));
    }
}
