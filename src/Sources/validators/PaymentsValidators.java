package validators;

import pages.PaymentsPage;

public class PaymentsValidators {
    private PaymentsPage page;
    private static String messageElement = "The element name does not match the expected";
    private static String messageUrl     = "Current url does not match the expected";

    public PaymentsValidators(PaymentsPage page) {
        this.page = page;
    }

    /**
     * Validate default state of Zhku-Moscow page
     * @return String
     */
    public StringBuilder isPaymentsMenuCorrect(String url) {
        StringBuilder errors = new StringBuilder();
        String[] expectedStrs = {
                "Мобильная связь",
                "ЖКХ",
                "Интернет",
                "Сетевой маркетинг",
                "Домашний телефон",
                "Телевидение",
                "Госуслуги",
                url + "/payments/",
        };
        String[] actualStrs = {
                page.getPaymentsMenuItemName("/mobilnaya-svyaz/"),
                page.getPaymentsMenuItemName("/kommunalnie-platezhi/"),
                page.getPaymentsMenuItemName("/internet/"),
                page.getPaymentsMenuItemName("/setevoi-marketing/"),
                page.getPaymentsMenuItemName("/domashnii-telefon/"),
                page.getPaymentsMenuItemName("/televidenie/"),
                page.getPaymentsMenuItemName("/state-services/"),
                page.driver.getCurrentUrl(),
        };
        errors.append(_isPaymentsMenuCorrect(errors, expectedStrs, actualStrs));
        return errors;
    }

    private StringBuilder _isPaymentsMenuCorrect(StringBuilder errors, String[] expectedStrs, String[] actualStrs) {
        for (int i = 0; i < expectedStrs.length; i++) {
            String expected = expectedStrs[i];
            String actual   = actualStrs[i].replace("&nbsp;"," ");
            if (!expected.equals(actual)) {
                errors.append(String.format(i == 7 ? messageUrl : messageElement + "\n" +
                                            "Expected: %s\n" +
                                            "Actual: %s\n", expected, actual));
            }
        }
        return errors;
    }
}