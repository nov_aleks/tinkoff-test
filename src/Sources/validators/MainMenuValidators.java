package validators;

import elements.MainMenuElement;

public class MainMenuValidators {
    private MainMenuElement page;
    private static String messageElement = "The element name does not match the expected";

    public MainMenuValidators(MainMenuElement page) {
        this.page = page;
    }

    /**
     * Validate default state of Zhku-Moscow page
     * @return String
     */
    public StringBuilder isMainMenuCorrect() {
        StringBuilder errors = new StringBuilder();
        String[] expectedStrs = {
                "КАРТЫ",
                "ВКЛАДЫ",
                "ПЛАТЕЖИ",
                "ИНВЕСТИЦИИ",
                "ИПОТЕКА",
                "КРЕДИТ",
                "МОБАЙЛ",
                "СТРАХОВАНИЕ",
                "ПУТЕШЕСТВИЯ",
        };
        String[] actualStrs = {
                page.getMenuItemName("/cards/credit-cards/"),
                page.getMenuItemName("/deposit/"),
                page.getMenuItemName("/payments/"),
                page.getMenuItemName("/invest/"),
                page.getMenuItemName("/loans/mortgage/"),
                page.getMenuItemName("/loans/cash-loan/"),
                page.getMenuItemName("/mobile-operator/"),
                page.getMenuItemName("/insurance/"),
                page.getMenuItemName("https://travel.tinkoff.ru/"),
        };
        errors.append(_isPageCorrect(errors, expectedStrs, actualStrs));
        return errors;
    }

    private StringBuilder _isPageCorrect(StringBuilder errors, String[] expectedStrs, String[] actualStrs) {
        for (int i = 0; i < expectedStrs.length; i++) {
            String expected = expectedStrs[i];
            String actual   = actualStrs[i].replace("&nbsp;"," ");
            if (!expected.equals(actual)) {
                errors.append(String.format(messageElement + "\n" +
                                            "Expected: %s\n" +
                                            "Actual: %s\n", expected, actual));
            }
        }
        return errors;
    }
}