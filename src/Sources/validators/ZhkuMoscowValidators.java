package validators;

import org.openqa.selenium.WebElement;
import pages.ZhkuMoscowPage;

import java.util.List;

public class ZhkuMoscowValidators {
    private ZhkuMoscowPage page;
    private static String messageElement = "The element name does not match the expected";
    private static String messageUrl     = "Current url does not match the expected";

    public ZhkuMoscowValidators(ZhkuMoscowPage page) {
        this.page = page;
    }

    /**
     * Validate default state of Zhku-Moscow page
     * @return String
     */
    public StringBuilder isDefaultPageCorrect(String url) {
        StringBuilder errors = new StringBuilder();
        String[] expectedStrs = {
                "Узнайте задолженность по ЖКУ в Москве",
                "УЗНАТЬ ЗАДОЛЖЕННОСТЬ ЗА ЖКУ В МОСКВЕ",
                "ОПЛАТИТЬ ЖКУ В МОСКВЕ",
                url + "/zhku-moskva/",
        };
        String[] actualStrs = {
                page.getPageTitle("Узнайте задолженность по ЖКУ в Москве"),
                page.getActiveTabTitle("Узнать задолженность за ЖКУ в Москве"),
                page.getTabTitle("Оплатить ЖКУ в Москве"),
                page.driver.getCurrentUrl(),
        };
        errors.append(_isPageCorrect(errors, expectedStrs, actualStrs));
        errors.append(_isLiabilityFormPlaceholdersCorrect(errors));
        return errors;
    }

    /**
     * Validate payment tab of Zhku-Moscow page
     * @return String
     */
    public StringBuilder isPaymentTabCorrect(String url) {
        StringBuilder errors = new StringBuilder();
        String[] expectedStrs = {
                "Оплатите ЖКУ в Москве без комиссии",
                "УЗНАТЬ ЗАДОЛЖЕННОСТЬ ЗА ЖКУ В МОСКВЕ",
                "ОПЛАТИТЬ ЖКУ В МОСКВЕ",
                url + "/zhku-moskva/oplata/?tab=pay",
        };
        String[] actualStrs = {
                page.getPageTitle("Оплатите ЖКУ в Москве без комиссии"),
                page.getTabTitle("Узнать задолженность за ЖКУ в Москве"),
                page.getActiveTabTitle("Оплатить ЖКУ в Москве"),
                page.driver.getCurrentUrl(),
        };
        errors.append(_isPageCorrect(errors, expectedStrs, actualStrs));
        errors.append(_isPaymentFormPlaceholdersCorrect(errors));
        return errors;
    }

    /**
     * Validate liability form placeholders
     * @return String
     */
    private StringBuilder _isLiabilityFormPlaceholdersCorrect(StringBuilder errors) {
        String[] expectedPlaceholders = {
                "Код плательщика за ЖКУ в Москве",
                "Введите e-mail",
                "Логин или телефон",
        };
        return _formPlaceholdersValidator(errors, expectedPlaceholders);
    }

    /**
     * Validate payment form placeholders
     * @return String
     */
    private StringBuilder _isPaymentFormPlaceholdersCorrect(StringBuilder errors) {
        String[] expectedPlaceholders = {
                "Код плательщика за ЖКУ в Москве",
                "За какой период оплачиваете коммунальные услуги",
                "Сумма добровольного страхования жилья из квитанции за ЖКУ в Москве, \u20BD",
                "Сумма платежа, \u20BD",
        };
        return _formPlaceholdersValidator(errors, expectedPlaceholders);
    }

    private StringBuilder _isPageCorrect(StringBuilder errors, String[] expectedStrs, String[] actualStrs) {
        for (int i = 0; i < expectedStrs.length; i++) {
            String expected = expectedStrs[i];
            String actual   = actualStrs[i].replace("&nbsp;"," ");
            if (!expected.equals(actual)) {
                errors.append(String.format(i == 3 ? messageUrl : messageElement + "\n" +
                                            "Expected: %s\n" +
                                            "Actual: %s\n", expected, actual));
            }
        }
        return errors;
    }

    private StringBuilder _formPlaceholdersValidator(StringBuilder errors, String[] expectedStrs) {
        List<WebElement> actualStrs = page.driver.findElements(page.locators.getElement(page.locators.formPlaceholder));
        for (int i = 0; i < expectedStrs.length; i++) {
            String expected = expectedStrs[i];
            String actual   = actualStrs.get(i).getText().replace("&nbsp;"," ");
            if (!expected.equals(actual)) {
                errors.append(String.format(messageElement + "\n" +
                                            "Expected: %s\n" +
                                            "Actual: %s\n", expected, actual));
            }
        }
        return errors;
    }
}